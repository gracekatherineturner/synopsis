#!/bin/bash
folder=`pwd`
export PYTHONPATH="$folder"
echo "Running Tests..."
python3 tests/testTopicFinder.py
read -p "Did all tests pass? (Y/N): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
read -p "Yay! Did you update the readme (if needed)? (Y/N): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
read -p "Yay! Did you update the version in setup.cfg? (Y/N): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
python3 setup.py sdist bdist_wheel
git add "topicfinder/__init__.py"
git add "topicfinder/topicfinder.py"
git add "tests/testTopicFinder.py"
git add "commitchecklist.sh"
git add "README.md"
git add "setup.py"
git add "pyproject.toml"
git add "setup.cfg"
read -p "Commit message?" message
git commit -m "$message"
read -p "You sure you want me to push this (Y/N): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
git push
unset PYTHONPATH
read -p "Do you want me to upload to twine?" confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
read -p "What is the new version?" version
python3 -m twine upload "dist/topicfinder-$version*"
pip3 install --upgrade topicfinder
