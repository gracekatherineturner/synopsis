import unittest
from topicfinder import *

class TestSynopsis(unittest.TestCase):
    def test_getTokenLists(self):
        with self.assertRaises(Exception):
            getTokenLists("boo")
        with self.assertRaises(Exception):
            getTokenLists([1,3])
        ret = getTokenLists([{"text": "boo"}])
        self.assertEqual(ret, [[{'startIndex': 0, 'endIndex': 3, 'token': 'boo', 'tokenType': 'token', 'index': 0, 'parent': -1, 'docID': 0}]])
        ret = getTokenLists([{"text": "boo", "docID": "123"}])
        self.assertEqual(ret, [[{'startIndex': 0, 'endIndex': 3, 'token': 'boo', 'tokenType': 'token', 'index': 0, 'parent': -1, 'docID': "123"}]])
        ret = getTokenLists([{"text": "boo", "docID": "123"}, {"text": "boo", "docID": "bah"}])
        self.assertEqual(ret, [[{'startIndex': 0, 'endIndex': 3, 'token': 'boo', 'tokenType': 'token', 'index': 0, 'parent': -1, 'docID': '123'}], [{'startIndex': 0, 'endIndex': 3, 'token': 'boo', 'tokenType': 'token', 'index': 0, 'parent': -1, 'docID': 'bah'}]])
        ret = getTokenLists([{"text": "boom pow, sound of my heart", "docID": "123"}])
        self.assertEqual(ret, [[{'startIndex': 0, 'endIndex': 4, 'token': 'boom', 'tokenType': 'token', 'index': 0, 'parent': -1, 'docID': '123'}, {'startIndex': 4, 'endIndex': 5, 'token': ' ', 'tokenType': 'noise', 'index': 1, 'parent': -1, 'docID': '123'}, {'startIndex': 5, 'endIndex': 8, 'token': 'pow', 'tokenType': 'token', 'index': 2, 'parent': -1, 'docID': '123'}, {'startIndex': 8, 'endIndex': 10, 'token': ', ', 'tokenType': 'noise', 'index': 3, 'parent': -1, 'docID': '123'}, {'startIndex': 10, 'endIndex': 15, 'token': 'sound', 'tokenType': 'token', 'index': 4, 'parent': 3, 'docID': '123'}, {'startIndex': 10, 'endIndex': 27, 'token': 'sound of my heart', 'tokenType': 'token', 'index': 5, 'parent': 3, 'docID': '123'}, {'startIndex': 15, 'endIndex': 22, 'token': ' of my ', 'tokenType': 'noise', 'index': 6, 'parent': 3, 'docID': '123'}, {'startIndex': 22, 'endIndex': 27, 'token': 'heart', 'tokenType': 'token', 'index': 7, 'parent': 3, 'docID': '123'}]])
    def test_strip(self):
        with self.assertRaises(Exception):
            stripWord(1)
        ret = stripWord("test")
        self.assertEqual(ret, "test")
        ret = stripWord("testing")
        self.assertEqual(ret, "test")
        ret = stripWord("testion")
        self.assertEqual(ret, "tes")
        ret = stripWord("tession")
        self.assertEqual(ret, "tes")
        ret = stripWord("ingtest")
        self.assertEqual(ret, "ingtest")

    def test_isTheSameWord(self):
        self.assertEqual(isTheSameWord("ing", "ing"), True),
        self.assertEqual(isTheSameWord("", "Test"), False),
        self.assertEqual(isTheSameWord("test", ""), False),
        self.assertEqual(isTheSameWord("test", "Test"), True)
        self.assertEqual(isTheSameWord("test", "Testing"), True)
        self.assertEqual(isTheSameWord("atest", "Test"), False)
        self.assertEqual(isTheSameWord("testoc", "Test"), True)
        self.assertEqual(isTheSameWord("testoc", "Toot", 0.9), False)
        self.assertEqual(isTheSameWord("tes", "Tes", 0.9), True)
        self.assertEqual(isTheSameWord("toos", "Tesing", 0.75), False)
        self.assertEqual(isTheSameWord("tes", "Test", 0.75), False)

    def test_phrases(self):
        ret = getPhrases(getTokenLists([{"text": "boo"}]))
        self.assertEqual(ret,[('boo', [{'startIndex': 0, 'endIndex': 3, 'token': 'boo', 'tokenType': 'token', 'index': 0, 'parent': -1, 'docID': 0}])] )
        ret = getPhrases(getTokenLists([{"text": "bounce"},{"text": "bounce bounces"}]))
        self.assertEqual(ret, [('bounce', [{'startIndex': 0, 'endIndex': 6, 'token': 'bounce', 'tokenType': 'token', 'index': 0, 'parent': -1, 'docID': 0}, {'startIndex': 0, 'endIndex': 6, 'token': 'bounce', 'tokenType': 'token', 'index': 0, 'parent': -1, 'docID': 1}]), ('bounce bounces', [{'startIndex': 0, 'endIndex': 14, 'token': 'bounce bounces', 'tokenType': 'token', 'index': 1, 'parent': -1, 'docID': 1}]), ('bounces', [{'startIndex': 7, 'endIndex': 14, 'token': 'bounces', 'tokenType': 'token', 'index': 3, 'parent': -1, 'docID': 1}])])
    def test_groupPhrasesBySimilarity(self):
        docs = [{"text": "bounce"},{"text": "bounce bounces"}]
        ret = groupPhrasesBySimilarity(getPhrases(getTokenLists(docs)))
        self.assertEqual(ret,[{'phrases': ['bounce', 'bounces'], 'tokens': [{'startIndex': 0, 'endIndex': 6, 'token': 'bounce', 'tokenType': 'token', 'index': 0, 'parent': -1, 'docID': 0}, {'startIndex': 0, 'endIndex': 6, 'token': 'bounce', 'tokenType': 'token', 'index': 0, 'parent': -1, 'docID': 1}, {'startIndex': 7, 'endIndex': 14, 'token': 'bounces', 'tokenType': 'token', 'index': 3, 'parent': -1, 'docID': 1}]}, {'phrases': ['bounce bounces'], 'tokens': [{'startIndex': 0, 'endIndex': 14, 'token': 'bounce bounces', 'tokenType': 'token', 'index': 1, 'parent': -1, 'docID': 1}]}])
        self.assertEqual(synopsis(docs), ret)
        self.assertEqual(synopsis(docs, 0.8), ret)
        docs = [{"text": "123 123"}, {"text": "1232123 boo no!"}]
        ret = groupPhrasesBySimilarity(getPhrases(getTokenLists(docs)))
        self.assertEqual(ret, [{'phrases': ['numeric'], 'tokens': [{'startIndex': 0, 'endIndex': 3, 'token': '123', 'tokenType': 'numeric', 'index': 0, 'parent': -1, 'docID': 0}, {'startIndex': 4, 'endIndex': 7, 'token': '123', 'tokenType': 'numeric', 'index': 2, 'parent': -1, 'docID': 0}, {'startIndex': 0, 'endIndex': 7, 'token': '1232123', 'tokenType': 'numeric', 'index': 0, 'parent': -1, 'docID': 1}]}, {'phrases': ['boo'], 'tokens': [{'startIndex': 8, 'endIndex': 11, 'token': 'boo', 'tokenType': 'token', 'index': 2, 'parent': -1, 'docID': 1}]}, {'phrases': ['negation'], 'tokens': [{'startIndex': 12, 'endIndex': 14, 'token': 'no', 'tokenType': 'negation', 'index': 4, 'parent': -1, 'docID': 1}]}])
    def test_topicfinder(self):
        doc = {"text": "bounces"}
        topics = []
        ret = topicfinder(doc, topics)
        self.assertEqual(ret, [{'endIndex': 7, 'index': 0,'parent': -1,'startIndex': 0,'token': 'bounces','tokenType': 'token','topic': {}}])
        doc = {"text": "no bounces"}
        topics=[{"unit": "TEST", "value": "BOUNCE", "token": "bounce", "frequency": 10}]
        ret = topicfinder(doc, topics)
        self.assertEqual(ret, [{'startIndex': 0, 'endIndex': 2, 'token': 'no', 'tokenType': 'negation', 'index': 0, 'parent': -1, 'topic': {}}, {'startIndex': 2, 'endIndex': 3, 'token': ' ', 'tokenType': 'noise', 'index': 1, 'parent': -1, 'topic': {}}, {'startIndex': 3, 'endIndex': 10, 'token': 'bounces', 'tokenType': 'token', 'index': 2, 'parent': -1, 'topic': {'unit': 'TEST', 'value': 'BOUNCE', 'token': 'bounce', 'frequency': 10}}])

if __name__ == '__main__':
    unittest.main()
